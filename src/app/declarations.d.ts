/* 
 * This file is used to hold ambient type declarations and
 * type shims for npm modules without type declarations.
 * 
 * E.g.
 * 
 * declare module "package-without-declarations";
 */
